package client;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import com.kis.pojo.user.User;
import com.kis.service.user.UserService;


public class TestRMI {
	public static void main(String args[]){
        try {
            UserService us=(UserService) Naming.lookup("rmi://192.168.12.121:8090/userService");
            //在RMI服务注册表中查找名称为RHello的对象，并调用其上的方法
            User u = new User();
            u.setUsername("wongloong");
            u.setLoginname("diarbao");
            for (int i = 0; i < 100; i++) {
                System.out.println(us.getClass());
               u.setUsername(u.getUsername()+i);
               System.out.println(us.addUser(u));
            }
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();  
        }
    } 
}
