package com.kis.controller.index;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.kis.pojo.user.User;

@Controller
public class IndexController {
	@RequestMapping("/index")
	public String Index() {
		return "index";
	}

	@RequestMapping("/login")
	public String login(User u, HttpServletRequest request) {
		String result = "";
		if (request.getMethod().toUpperCase().equals("GET")) {
			result = "login";
		} else {
			System.out.println(u.getLoginname());
			System.out.println(u.getPassword());
			result = "index";
		}
		return result;
	}
}
