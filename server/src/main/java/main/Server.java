package main;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.Map;

import com.kis.core.Core;
import com.kis.core.Utils;
import com.kis.core.emnu.PropertiesType;

public class Server {
	public static void main(String[] args) {
		new Core().run();
	}
}
