package com.kis.controller.index;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kis.core.Core;
import com.kis.pojo.user.User;

@Controller
public class IndexController {
    private Map<String, Object> result = new HashMap<String, Object>();
    private Core core = new Core();
    @RequestMapping("/")
    public String GetIndex() {
        return "redirect:/index";
    }

    @RequestMapping("/index")
    public String Index() {
        return "index";
    }

    @RequestMapping("login")
    public String login(User u, HttpServletRequest request) {
        String result = "";
        if (request.getMethod().toUpperCase().equals("GET")) {
            result = "login";
        } else {
            System.out.println(u.getLoginname());
            System.out.println(u.getPassword());
            request.getSession().setAttribute("user", u);
            result = "index";
        }
        return result;
    }

    @ResponseBody
    @RequestMapping("/index/startup")
    public Map<String, Object> startup() {
        result.clear();
        result=core.run();
//        result = new Core().run();
        return result;
    }

    @ResponseBody
    @RequestMapping("/index/stopRMI")
    public Map<String, Object> stopRMi() {
        result.clear();
        result=core.stop();
//        result = new Core().stop();
        return result;
    }
}
