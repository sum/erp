package com.kis.core.emnu;

public enum PropertiesType {
	ADDRESS("address"),SCANPACKAGE("scan_package"),PORT("port");
	private String value;

	private PropertiesType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	
	
}
