package com.kis.service.user.impl;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import com.kis.pojo.user.User;
import com.kis.service.user.UserService;

public class UserServiceImpl extends UnicastRemoteObject implements UserService {
	public UserServiceImpl() throws RemoteException {
		super();
	}
	public String Test() {
		return "Hello Test";
	}

	public String addUser(User u) {
		System.out.println(u.toString());
		return "Add Success";
	}

}
