package com.kis.service.user;

import java.rmi.Remote;
import java.rmi.RemoteException;

import com.kis.pojo.user.User;

public interface UserService extends Remote{
	public String Test() throws RemoteException; 
	public String addUser(User u) throws RemoteException;
}
